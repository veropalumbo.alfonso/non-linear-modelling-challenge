import numpy as np
import matplotlib.pyplot as plt
import CosmoBolognaLib as cbl
import os 
from matplotlib import rc
from scipy.special import loggamma, gamma
from scipy.fft import rfft, irfft, fft
from scipy.interpolate import interp1d

class PkFFTlog:
    def __init__(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):

        self.delta = None
        self.k_n = None
        self.k_n_bias = None
        self.eta_m = None
        self.nu_m = None

        self.Nmax = None
        self.bias = None
        self.kmin0 = None
        self.kmax0 = None
        self.k_eta_matrix = None
        self.set_parameters(bias=bias, Nmax=Nmax, kmin0=kmin0, kmax0=kmax0)

    def set_parameters(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):

        self.Nmax = Nmax
        self.bias = bias
        self.kmin0 = kmin0
        self.kmax0 = kmax0

        self.delta = 1 / (self.Nmax - 1) * np.log(self.kmax0 / self.kmin0)
        self.k_n = np.array(
            [self.kmin0 * np.exp(i * self.delta) for i in range(self.Nmax)])
        self.eta_m = np.array([
            2 * np.pi / (self.Nmax * self.delta) *
            (j - self.Nmax / 2) for j in range(0, self.Nmax + 1)
        ])
        self.nu_m = -0.5*(self.bias+1j*self.eta_m)
        self.k_n_bias = np.array(
            [np.exp(-self.bias * i * self.delta) for i in range(self.Nmax)])
        X, Y = np.meshgrid(self.k_n, -2*self.nu_m)
        self.k_nu_matrix = X**Y

    def _get_II(self, nu1, nu2):
        return 1. / (8 * np.pi**1.5) * gamma(1.5 - nu1) * gamma(
            1.5 - nu2) * gamma(nu1 + nu2 - 1.5) / (gamma(nu1) * gamma(nu2) *
                                                   gamma(3 - nu1 - nu2))
    def get_c_m(self, p_k):

        pk_bias = p_k * self.k_n_bias

        cm = rfft(pk_bias) / self.Nmax
        c_m = 1j * np.zeros(self.Nmax + 1)
        c_m[int(self.Nmax / 2):] = cm
        c_m[0:int(self.Nmax / 2)] = cm[-1:0:-1].conj()

        return c_m * self.kmin0**(2*self.nu_m)

class PkMatter(PkFFTlog):
        
    def __init__(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):
        super().__init__(bias=bias, Nmax=Nmax, kmin0=kmin0, kmax0=kmax0)
            
        self.M22 = None
        self.M13 = None
        
    def _get_M13(self, nu1):
        return (1 + 9 * nu1) / 4 * np.tan(nu1 * np.pi) / (28 * np.pi *
                                                          (nu1 + 1) * nu1 *
                                                          (nu1 - 1) *
                                                          (nu1 - 2) *
                                                          (nu1 - 3))

    def _get_M22(self, nu1, nu2):
        nu12 = nu1 + nu2
        den = 196 * nu1 * (1 + nu1) * (0.5 - nu1) * nu2 * (1 + nu2) * (0.5 -
                                                                       nu2)
        num = nu1 * nu2 * (98 * nu12**2 - 14 * nu12 +
                           36) - 91 * nu12**2 + 3 * nu12 + 58
        num = (1.5 - nu12) * (0.5 - nu12) * num * self.II
        return num / den

    def set_tables(self):
        X, Y = np.meshgrid(self.nu_m, self.nu_m)
        self.II = self._get_II(X, Y)
        self.M22 = self._get_M22(X, Y)
        self.M13 = self._get_M13(X)

    def __call__(self, interp_pk):

        p_k = interp_pk(self.k_n)
        c_m = self.get_c_m(p_k)
        
        #p_k_13 = self.k_n**3 * p_k * np.dot(c_m * self.M13, self.k_nu_matrix).real
        p_k_13 = self.k_n**3 * np.array([
            np.dot(c_m * line, np.dot(self.M13, c_m * line)).real
            for line in self.k_nu_matrix.T
        ])
        
        p_k_22 = self.k_n**3 * np.array([
            np.dot(c_m * line, np.dot(self.M22, c_m * line)).real
            for line in self.k_nu_matrix.T
        ])

        return self.k_n, p_k, p_k_22, p_k_13
    
class PkBias(PkFFTlog):
    def __init__(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):
        super().__init__(bias=bias, Nmax=Nmax, kmin0=kmin0, kmax0=kmax0)

        self.Md2 = None
        self.Mg2 = None
        self.MFg2 = None
        self.Md2d2 = None
        self.Mg2g2 = None
        self.Md2g2 = None
        self.kernels = ["d2", "g2", "d2d2", "g2g2", "d2g2"]

    def _get_Md2(self, nu1, nu2):
        nu12 = nu1 + nu2

        return (3 - 2 * nu12) * (4 - 7 * nu12) / (14 * nu1 * nu2) * self.II

    def _get_Mg2(self, nu1, nu2):
        nu12 = nu1 + nu2

        return -(3 - 2 * nu12) * (1 - 2 * nu12) * (6+7*nu12)/ (28 * nu1 * (1+nu1) * nu2 * (1+nu2)) * self.II

    def _get_MFg2(self, nu1):
        return -15 * np.tan(nu1 * np.pi) / (28 * np.pi * (nu1 + 1) * nu1 *
                                            (nu1 - 1) * (nu1 - 2) * (nu1 - 3))

    def _get_Md2d2(self, nu1, nu2):
        return 2 * self.II

    def _get_Mg2g2(self, nu1, nu2):
        nu12 = nu1 + nu2

        return (3 - 2 * nu12) * (1 - 2 * nu12) / ((nu1) * (1 + nu1) * nu2 *
                                                  (1 + nu2)) * self.II
    
    def _get_Md2g2(self, nu1, nu2):
        nu12 = nu1 + nu2
        return (3 - 2*nu12) / (nu1 * nu2) * self.II

    def set_tables(self):
        X, Y = np.meshgrid(self.nu_m, self.nu_m)
        self.II = self._get_II(X, Y)

        self.MFg2 = self._get_MFg2(X) #-0.5 * self.eta_m)
    
        for kn in self.kernels:
            setattr(self, "M%s" % kn, getattr(self, "_get_M%s"%kn)(X, Y))
            
    def __call__(self, interp_pk):
        
        p_k = interp_pk(self.k_n)
        c_m = self.get_c_m(p_k)
        
        pk_d2 = self.k_n**3 * np.array([np.dot(c_m * line,\
                                np.dot(self.Md2, c_m * line)).real\
                         for line in self.k_nu_matrix.T])
        
        pk_g2 = self.k_n**3 * np.array([np.dot(c_m * line,\
                                np.dot(self.Mg2, c_m * line)).real\
                         for line in self.k_nu_matrix.T])
        
        pk_22 = self.k_n**3 * np.array([np.dot(c_m * line,\
                                np.dot(self.Md2d2, c_m * line)).real\
                         for line in self.k_nu_matrix.T])
        
        pk_d2g2 = self.k_n**3 * np.array([np.dot(c_m * line,\
                                np.dot(self.Md2g2, c_m * line)).real\
                         for line in self.k_nu_matrix.T])     
        
        pk_g2g2 = self.k_n**3 * np.array([np.dot(c_m * line,\
                                np.dot(self.Mg2g2, c_m * line)).real\
                         for line in self.k_nu_matrix.T]) 
        
        pk_Fg2 = self.k_n**3 * np.array([np.dot(c_m * line,\
                                np.dot(self.MFg2, c_m * line)).real\
                         for line in self.k_nu_matrix.T])
        
        #p_k_Fg2 = self.k_n**3 * p_k * np.dot(c_m * self.MFg2, self.k_nu_matrix).real
                
        return self.k_n, pk_d2, pk_g2, pk_22-pk_22[0], pk_d2g2, pk_g2g2, pk_Fg2
    
class XiMatter(PkMatter):
    def __init__(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):
        super().__init__(bias=bias, Nmax=Nmax, kmin0=kmin0, kmax0=kmax0)
        
        self.omega_m = None
        self.Mtilde_11 = None
        self.Mtilde_13 = None
        self.Mtilde_22 = None

    def set_tables(self):
        super().set_tables()
        self.omega_m = self.nu_m - 1.5
        
        X, Y = np.meshgrid(self.nu_m, self.nu_m)
        XY = X+Y
        
        self.Mtilde_11 = 1. / (2 * np.pi**2) * gamma(2 - 2 * self.nu_m) *\
                        np.sin(np.pi * self.nu_m)
        
        self.Mtilde_13 = 1. / (2 * np.pi**2) * gamma(5 - 2 * XY) *\
                        np.cos(np.pi * XY) * self.M13
        
        
        self.Mtilde_22 = 1. / (2 * np.pi**2) * gamma(5 - 2 * XY) *\
                        np.cos(np.pi * XY) * self.M22

    def __call__(self, rad, interp_pk):

        p_k = interp_pk(self.k_n)
        c_m = self.get_c_m(p_k)
        
        X, Y = np.meshgrid(rad, 2*self.omega_m)
        romega = X**Y
        return np.dot(c_m * self.Mtilde_11, romega).real

    def first_loops(self, rad, interp_pk):

        p_k = interp_pk(self.k_n)
        c_m = self.get_c_m(p_k)
        
        X, Y = np.meshgrid(rad, 2*self.omega_m)
        romega = X**Y
       
        xi13 = np.array([
            np.dot(c_m * line, np.dot(self.Mtilde_13, c_m * line)).real
            for line in romega.T
        ])
        
        xi22 = np.array([
            np.dot(c_m * line, np.dot(self.Mtilde_22, c_m * line)).real
            for line in romega.T
        ])
        
        return xi13, xi22
    
class XiBias(PkBias):
    def __init__(self, bias=-1.6, Nmax=256, kmin0=1.e-5, kmax0=100):
        super().__init__(bias=bias, Nmax=Nmax, kmin0=kmin0, kmax0=kmax0)
        
        self.omega_m = None
        self.Mtilde_d2 = None
        self.Mtilde_g2 = None
        self.Mtilde_Fg2 = None
        self.Mtilde_d2d2 = None
        self.Mtilde_g2g2 = None
        self.Mtilde_d2g2 = None
        
    def set_tables(self):
        super().set_tables()
        self.omega_m = self.nu_m - 1.5
        
        X, Y = np.meshgrid(self.nu_m, self.nu_m)
        XY = X+Y
        Mtilde = 1. / (2 * np.pi**2) * gamma(5 - 2 * XY) *\
                        np.cos(np.pi * XY)
        
        for kn in self.kernels+["Fg2"]:
            setattr(self, "Mtilde_%s"%kn, getattr(self, "M%s"%kn) * Mtilde )      
        
        self.Mtilde_11 = 1. / (2 * np.pi**2) * gamma(2- 2 * (self.nu_m-1)) *\
                        np.sin(np.pi * (self.nu_m-1))
        
    def __call__(self, rad, interp_pk):

        p_k = interp_pk(self.k_n)
        c_m = self.get_c_m(p_k)
        
        X, Y = np.meshgrid(rad, 2*self.omega_m)
        romega = X**Y

        xi_d2 = np.array([np.dot(c_m * line,\
                                np.dot(self.Mtilde_d2, c_m * line)).real\
                         for line in romega.T])
        
        xi_g2 = np.array([np.dot(c_m * line,\
                                np.dot(self.Mtilde_g2, c_m * line)).real\
                         for line in romega.T])
        
        xi_22 = np.array([np.dot(c_m * line,\
                                np.dot(self.Mtilde_d2d2, c_m * line)).real\
                         for line in romega.T])
        
        xi_d2g2 = np.array([np.dot(c_m * line,\
                                np.dot(self.Mtilde_d2g2, c_m * line)).real\
                         for line in romega.T])     
        
        xi_g2g2 = np.array([np.dot(c_m * line,\
                                np.dot(self.Mtilde_g2g2, c_m * line)).real\
                         for line in romega.T]) 
        
        xi_Fg2 = np.array([np.dot(c_m * line,\
                                np.dot(self.Mtilde_Fg2, c_m * line)).real\
                         for line in romega.T])
        
        X, Y = np.meshgrid(rad, 2*(self.omega_m-1))
        romega = X**Y
        xi_ctr = -np.dot(self.Mtilde_11 * romega.T, c_m)
                
        return xi_d2, xi_g2, xi_22, xi_d2g2, xi_g2g2, xi_Fg2, xi_ctr.real
