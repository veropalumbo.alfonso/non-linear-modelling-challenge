import numpy as np
from scipy.interpolate import CubicSpline
from manage_measures import snap_to_str, check_snapshot
import matplotlib.pyplot as plt

class PowerSpectrumModel:

    def __init__(self, snap):

        #Interpolation
        self.iPlin = None 
        self.iPmLO = None 
        self.iPmNLO = None 
        self.iPb1b2 = None 
        self.iPb1bG2 = None 
        self.iPb1bGamma3 = None 
        self.iPb2b2 = None 
        self.iPb2bG2 = None 
        self.iPbG2bG2 = None 

        self.set(snap)

    def set(self, snap):

        zz = snap_to_str[check_snapshot(snap)]
        root_dir = "MarcoDB/"
        table = root_dir+'power_galaxy_FS_z%s_hres_1-loop_IRres_withoutpi_FFTlogQFT_MarcoDB.dat'%zz
        
        k, Plin, PmLO, PmNLO, Pb1b2, Pb1bG2, Pb1bGamma3, Pb2b2, Pb2bG2, PbG2bG2 = np.loadtxt(table,unpack=True)

        #Interpolation
        self.iPlin = CubicSpline(k, Plin)
        self.iPmLO = CubicSpline(k, PmLO)
        self.iPmNLO = CubicSpline(k, PmNLO)
        self.iPb1b2 = CubicSpline(k, Pb1b2)
        self.iPb1bG2 = CubicSpline(k, Pb1bG2)
        self.iPb1bGamma3 = CubicSpline(k, Pb1bGamma3)
        self.iPb2b2 = CubicSpline(k, Pb2b2)
        self.iPb2bG2 = CubicSpline(k, Pb2bG2)
        self.iPbG2bG2 = CubicSpline(k, PbG2bG2)

    def __call__ (self, k, b1, b2, bG2, bGamma3, c0):

        # Here we can insert a warning if too small/large values of k are passed

        return b1**2 * self.iPmNLO(k) +\
               b1 * b2 * self.iPb1b2(k) +\
               b1 * bG2 * ( self.iPb1bG2(k) + 5/2 * self.iPb1bGamma3(k) ) +\
               b1 * bGamma3 * self.iPb1bGamma3(k) +\
               b2**2 * self.iPb2b2(k) +\
               b2 * bG2 * self.iPb2bG2(k) +\
               bG2**2 * self.iPbG2bG2(k) -\
               2 * c0 * k**2 * self.iPmLO(k)

if __name__ == "__main__":

    snap = "00045"

    power_spectrum = PowerSpectrumModel(snap)

    b1=1.3162;
    b2=-0.6294;
    bG2=-0.2062;
    bGamma3=0.2604;
    c0=2.5976;

    kk = np.logspace(np.log10(5e-4), 0, 1024)

    model = power_spectrum(kk, b1, b2, bG2, bGamma3, c0)

    figure, ax = plt.subplots(1, 1, figsize=(6, 5))
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.errorbar(kk, model, )
    ax.set_xlabel(r"$ k \, [h \, \mathrm{Mpc}^{-1}]$")
    ax.set_ylabel(r"$ P(k) $")
    ax.set_title("Snap %s"%snap)

    plt.show()

