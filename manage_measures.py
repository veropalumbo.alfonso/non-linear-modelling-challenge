import numpy as np
import matplotlib.pyplot as plt
import argparse

class ModelError(Exception):
    pass

class SnapshotError(Exception):
    pass

class PkBinningError(Exception):
    pass

def check_model (model):

    if ( (model==1) or (model==3)):
        return model
    else:
        raise ModelError

def check_snapshot (snap):

    if ( (snap=="00026") or (snap=="00030") or (snap=="00037") or (snap=="00045")):
        return snap
    else:
        raise SnapshotError

def check_binning (binning):
    if ( (binning==1) or (binning==2) or (binning==3)):
        return binning
    else:
        raise PkBinningError

bin_to_str = { 1 : "s1_c1", 2 : "s2_c1p5", 3 : "s3_c2" }

snap_to_str = { "00026" : "1p8", "00030" : "1p5", "00037" : "1p2", "00045" : "0p9"}

class FourierSpaceMeasure:

    def __init__(self, model, snap, binning, root_directory):
        self.model = check_model(model)
        self.snap = check_snapshot(snap)
        self.binning = check_binning(binning)
        
        self.path = root_directory+"gc-wp-nonlinear/flagship_measurements/snapshots_real_space/"

        self.nObjects = None
        self.inverse_density = None

    def read_properties(self, filename):

        with open(self.file_name) as f:
            first_line = f.readline()
        first_line = first_line.split()
        self.nObjects = float(first_line[0])
        self.inverse_density = float(first_line[1])

class PkMeasure(FourierSpaceMeasure):

    def __init__ (self, model, snap, binning, root_directory):
        FourierSpaceMeasure.__init__(self, model, snap, binning, root_directory)
        
        self.file_name = self.path+"power_spectrum/"+self.get_pk_filename()

        self.kk = None
        self.keff = None
        self.pk = None
        self.delta_pk = None

        self.read_properties(self.file_name)
        self.read_measure(self.file_name)

    def read_measure(self, filename):

        table = np.genfromtxt(self.file_name, unpack=True, skip_header=1)

        self.kk = table[0]
        self.keff = table[1]
        self.pk = table[2]
        self.delta_pk = np.sqrt(table[3])

    def get_pk_filename(self):
        return "power_flagship_mod%d_grid_768_bin_%s_z%s_full.dat"%(self.model, bin_to_str[self.binning], snap_to_str[self.snap])

    def __call__ (self, kmin, kmax):

        ww = np.where( (self.kk>=kmin) & (self.kk<=kmax))

        return self.pk[ww], self.delta_pk[ww], ww
  
class BkMeasure(FourierSpaceMeasure):

    def __init__ (self, model, snap, binning, root_directory):
        FourierSpaceMeasure.__init__(self, model, snap, binning, root_directory)

        self.file_name = self.path+"bispectrum/"+self.get_bk_filename()
        
        self.k12, self.k13, self.k23 = None, None, None
        self.keff12, self.keff13, self.keff23 = None, None, None
        self.pk12, self.pk13, self.pk23 = None, None, None
        self.bk = None
        self.delta_bk = None

        self.read_properties(self.file_name)
        self.read_measure(self.file_name)

    def read_measure(self, filename):

        table = np.genfromtxt(self.file_name, unpack=True, skip_header=1)
        self.k12, self.k13, self.k23 = table[0:3]
        self.keff12, self.keff13, self.keff23 = table[3:6]
        self.pk12, self.pk13, self.pk23= table[6:9]
        self.bk = table[9]
        self.delta_bk = np.sqrt(table[10])

    def get_bk_filename(self):
        return "bispectrum_flagship_mod%d_grid_540_bin_%s_z%s_full_kmax0p16.dat"%(self.model, bin_to_str[self.binning], snap_to_str[self.snap])

    def __call__ (self, triangle_condition, kind="keff"):

        ww = [triangle_condition(k12, k13, k23) for k12, k13, k23 in zip(self.k12, self.k13, self.k23)]

        return self.bk[ww], self.delta_bk[ww], np.array(ww)

if __name__ == "__main__":

    root_dir = "../"
    parser = argparse.ArgumentParser(description='Plot the flagship snapshot power spectrum')

    parser.add_argument('--model', type=int, required=True,
                        help='Halpha counts mode (it can be 1 or 3)')
    parser.add_argument('--snapshot', type=str, required=True,
                        help='Snapshot number')
    parser.add_argument('--binning', type=int, required=True,
                        help='power spectrum binning')
    parser.add_argument('--root_dir', type=str, default=root_dir)

    args = parser.parse_args()
    
    pk_measure = PkMeasure(args.model, args.snapshot, args.binning, args.root_dir)

    figure, ax = plt.subplots(1, 1, figsize=(6, 5))
    ax.set_xscale("log")
    ax.set_yscale("log")
    ax.errorbar(pk_measure.kk, pk_measure.pk, yerr=pk_measure.delta_pk, fmt="o", color="k")
    ax.set_xlabel(r"$ k \, [h \, \mathrm{Mpc}^{-1}]$")
    ax.set_ylabel(r"$ P(k) $")
    ax.set_title("Model %d - Snap %s - binning %d"%(args.model, args.snapshot, args.binning))

    plt.show()
    
            

