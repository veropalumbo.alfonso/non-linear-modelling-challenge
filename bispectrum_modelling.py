import numpy as np
from scipy.interpolate import CubicSpline
from manage_measures import snap_to_str, check_snapshot, check_binning, bin_to_str
import matplotlib.pyplot as plt

class BispectrumModel:

    def __init__(self, snap, binning, kind=""):

        self.k1, self.k2, self.k3 = None, None, None
        self.B_b1, self.B_b2, self.B_bg2 = None, None, None
        self.B_stoch_1, self.B_stoch_2 = None, None

        self.set(snap, binning, kind)

    def set(self, snap, binning, kind=""):
        zz = snap_to_str[check_snapshot(snap)]
        bin_str = bin_to_str[check_binning(binning)]
        
        root_dir = "MassimoG/Data/flagship_modelling/"
        table = root_dir+'bispectrum_tree_IR_%s_z%s_MassimoG%s.dat'%(bin_str, zz, kind)
        print(table)

        k1, k2, k3, B_b1, B_b2, B_bg2, B_stoch_1, B_stoch_2 = np.genfromtxt(table, unpack=True,\
                usecols=(0, 1, 2, 3, 4, 5, 6, 8), skip_header=1)

        self.k1, self.k2, self.k3 = k1, k2, k3
        self.B_b1, self.B_b2, self.B_bg2 = B_b1, B_b2, B_bg2
        self.B_stoch_1, self.B_stoch_2 = B_stoch_1, B_stoch_2

    def __call__ (self, b1, b2, bg2, alpha_1, alpha_2):

        B_ggg = b1**2 * (b1 * self.B_b1 + b2 * self.B_b2 + bg2 * self.B_bg2) 
        B_stoch = (1+alpha_1) * self.B_stoch_1 + (1+alpha_2) * self.B_stoch_2

        return B_ggg + B_stoch

if __name__ == "__main__":

    snap = "00045"
    binning = 1

    bispectrum = BispectrumModel(snap, binning)

    b1 = 1.3162
    b2 = -0.6294
    bG2 = -0.2062
    alpha_1 = 0
    alpha_2 = 0

    model = bispectrum(b1, b2, bG2, alpha_1, alpha_2)

    figure, ax = plt.subplots(1, 1, figsize=(6, 5))
    ax.set_xscale("linear")
    ax.set_yscale("log")
    ax.set_title("Snap %s - Bin %s"%(snap, binning))

    ax.set_xlabel(r"Triangle Index")
    ax.set_ylabel(r"$ B(k_{12}, k_{13}, k_{23}) $")

    ax.plot(model)

    plt.show()
