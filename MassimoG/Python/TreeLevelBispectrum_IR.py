# Coefficient b1^3
def C1Btree(k1, k2, k3):
    import numpy 
    return ( 5/7 + ( -5/14 * ( k1 )**( 2 ) * ( k2 )**( -2 ) + ( -5/14 * ( k1 \
)**( -2 ) * ( k2 )**( 2 ) + ( 3/14 * ( k1 )**( -2 ) * ( k3 )**( 2 ) + \
( 3/14 * ( k2 )**( -2 ) * ( k3 )**( 2 ) + 1/7 * ( k1 )**( -2 ) * ( k2 \
)**( -2 ) * ( k3 )**( 4 ) ) ) ) ) )

# Coefficient b1^2 b2
def C2Btree(k1, k2, k3):
    return 1

# Coefficient b1^2 g2
def C3Btree(k1, k2, k3):
    import numpy 
    return ( -2 + ( -1/2 * ( k1 )**( -2 ) * ( -1 * ( k1 )**( 2 ) + ( -1 * ( k2 \
)**( 2 ) + ( k3 )**( 2 ) ) ) + ( -1/2 * ( k2 )**( -2 ) * ( -1 * ( k1 \
)**( 2 ) + ( -1 * ( k2 )**( 2 ) + ( k3 )**( 2 ) ) ) + 1/2 * ( k1 )**( \
-2 ) * ( k2 )**( -2 ) * ( k3 )**( 2 ) * ( -1 * ( k1 )**( 2 ) + ( -1 * \
( k2 )**( 2 ) + ( k3 )**( 2 ) ) ) ) ) )

#Leading Order b1
def BLOb1(k1, k2, k3, rangek, PkTreeDM, PnwTreeDM, Sigma2):
    def BKernel(k1, k2, k3):
        return C1Btree(k1, k2, k3)
    import scipy
    from scipy.interpolate import CubicSpline
    Pw = CubicSpline(rangek, PkTreeDM - PnwTreeDM)
    Pnw = CubicSpline(rangek, PnwTreeDM)
    from math import exp
    return BKernel(k1, k2, k3)*(Pnw(k1)*Pnw(k2) + exp((-k1**2)*(Sigma2))*Pw(k1)*Pnw(k2) +  exp((-k2**2)*(Sigma2))*Pw(k2)*Pnw(k1)) +\
BKernel(k2, k3, k1)*(Pnw(k2)*Pnw(k3) + exp((-k2**2)*(Sigma2))*Pw(k2)*Pnw(k3) +  exp((-k3**2)*(Sigma2))*Pw(k3)*Pnw(k2)) +\
BKernel(k3, k1, k2)*(Pnw(k3)*Pnw(k1) + exp((-k3**2)*(Sigma2))*Pw(k3)*Pnw(k1) +  exp((-k1**2)*(Sigma2))*Pw(k1)*Pnw(k3))

#Leading Order b2
def BLOb2(k1, k2, k3, rangek, PkTreeDM, PnwTreeDM, Sigma2):
    def BKernel(k1, k2, k3):
        return C2Btree(k1, k2, k3)
    import scipy
    from scipy.interpolate import CubicSpline
    Pw = CubicSpline(rangek, PkTreeDM - PnwTreeDM)
    Pnw = CubicSpline(rangek, PnwTreeDM)
    from math import exp
    return BKernel(k1, k2, k3)*(Pnw(k1)*Pnw(k2) + exp((-k1**2)*(Sigma2))*Pw(k1)*Pnw(k2) +  exp((-k2**2)*(Sigma2))*Pw(k2)*Pnw(k1)) +\
BKernel(k2, k3, k1)*(Pnw(k2)*Pnw(k3) + exp((-k2**2)*(Sigma2))*Pw(k2)*Pnw(k3) +  exp((-k3**2)*(Sigma2))*Pw(k3)*Pnw(k2)) +\
BKernel(k3, k1, k2)*(Pnw(k3)*Pnw(k1) + exp((-k3**2)*(Sigma2))*Pw(k3)*Pnw(k1) +  exp((-k1**2)*(Sigma2))*Pw(k1)*Pnw(k3))

#Leading Order b3
def BLOg2(k1, k2, k3, rangek, PkTreeDM, PnwTreeDM, Sigma2):
    def BKernel(k1, k2, k3):
        return C3Btree(k1, k2, k3)
    import scipy
    from scipy.interpolate import CubicSpline
    Pw = CubicSpline(rangek, PkTreeDM - PnwTreeDM)
    Pnw = CubicSpline(rangek, PnwTreeDM)
    from math import exp
    return BKernel(k1, k2, k3)*(Pnw(k1)*Pnw(k2) + exp((-k1**2)*(Sigma2))*Pw(k1)*Pnw(k2) +  exp((-k2**2)*(Sigma2))*Pw(k2)*Pnw(k1)) +\
BKernel(k2, k3, k1)*(Pnw(k2)*Pnw(k3) + exp((-k2**2)*(Sigma2))*Pw(k2)*Pnw(k3) +  exp((-k3**2)*(Sigma2))*Pw(k3)*Pnw(k2)) +\
BKernel(k3, k1, k2)*(Pnw(k3)*Pnw(k1) + exp((-k3**2)*(Sigma2))*Pw(k3)*Pnw(k1) +  exp((-k1**2)*(Sigma2))*Pw(k1)*Pnw(k3))  

def B_hd_darkmatter(k1, k2, k3, rangek, PkTreeDM, PnwTreeDM, Sigma2):
    from scipy.interpolate import CubicSpline
    Pw = CubicSpline(rangek, PkTreeDM - PnwTreeDM)
    from math import exp
    Pnw = CubicSpline(rangek, PnwTreeDM)
    return C1Btree(k1, k2, k3)*Sigma2*((k2**2)*exp(-k2**2*Sigma2)*Pw(k2)*Pnw(k1) + (k1**2)*exp(-k1**2*Sigma2)*Pw(k1)*Pnw(k2)) +\
C1Btree(k3, k1, k2)*Sigma2*((k1**2)*exp(-k1**2*Sigma2)*Pw(k1)*Pnw(k3) + (k3**2)*exp(-k3**2*Sigma2)*Pw(k3)*Pnw(k1)) +\
C1Btree(k2, k3, k1)*Sigma2*((k3**2)*exp(-k3**2*Sigma2)*Pw(k3)*Pnw(k2) + (k2**2)*exp(-k2**2*Sigma2)*Pw(k2)*Pnw(k3))          
            
            
