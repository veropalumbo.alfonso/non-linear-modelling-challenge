
def MyCosmology(dataset):
    if dataset == "Flagship":
        import numpy
        data = numpy.genfromtxt('Data/flagship_linear_cb_hr_matterpower_z0p0.dat')
        import numpy as np
        k = np.array([data[i][0] for i in range(len(data))])
        PkTreeDM = np.array([data[i][1] for i in range(len(data))])
        k = np.insert(k, 0, 0)
        PkTreeDM = np.insert(PkTreeDM, 0, 0)
        return k, PkTreeDM

def MyNoWiggleCosmology(dataset):
    if dataset == "Flagship":
        import numpy
        data = numpy.genfromtxt('Data/flagship_linear_cb_hr_matterpower_nw_z0p0.dat')
        import numpy as np
        k = np.array([data[i][0] for i in range(1, len(data))])
        PkTreeDM = np.array([data[i][1] for i in range(1, len(data))])
        return k, PkTreeDM

def EuclidChallengePk(rangek, PkTreeDM):
    import numpy as np
    k = rangek[395:881]
    k = np.insert(k, 0, 0)
    Pk = PkTreeDM[395:881]
    Pk = np.insert(Pk, 0, 0)
    return k, Pk


def getFlagshipCosmology():
    hh = 0.67
    Omega_b = 0.049
    OmegaM = 0.319
    Omega_nu = 0.
    massless_neutrinos = 3.046
    massive_neutrinos = 0 
    Omega_radiation = 0.
    scalar_amp = 2.742e-9
    scalar_pivot = 0.002
    n_s = 0.96
    wa = 0.
    w0 = -1.   
    sigma8 = 0.83
    OmegaL = 1.-OmegaM

    cosmology = Cosmology(OmegaM, Omega_b, Omega_nu, massless_neutrinos, massive_neutrinos, OmegaL, Omega_radiation, hh, scalar_amp, scalar_pivot, n_s, w0, wa) 
    cosmology.set_sigma8(sigma8)

    return cosmology

def PkTreeLevel(kk, method, NL, redshift = 0.):

    # import Python modules for scientific computing and plotting 
    import numpy as np
    import import_ipynb
    import matplotlib.pyplot as plt

    #import the CosmoBolognaLib
    import CosmoBolognaLib as cbl
    from CosmoBolognaLib import Cosmology 

    import sys
    sys.path.insert(0, '/home/massimo/CosmoBolognaLib/Examples/clustering/output/')
    from ipynb.fs.full.FlagshipCosmology import getFlagshipCosmology as getFC

    # set the CosmoBolognaLib and the current directories
    cbl.SetDirs("/home/massimo/CosmoBolognaLib/", "./")

    # create an object of class Cosmology
    cosmo = getFC()   
    
    PkCAMB = np.asarray([cosmo.Pk(kk[i], method, NL, redshift) for i in range(len(kk))])
    #PkCAMB = cosmo.Pk(kk, method, NL, redshift)

    #PkCAMB = cosmo.xi_DM(kk[i], method, redshift, True, "test",NL)

    PkCAMB = [float(i) for i in PkCAMB]
    PkCAMB = np.asarray(PkCAMB)

    return PkCAMB

def Sigma2(k, Pnw):
    from scipy.interpolate import CubicSpline
    P = CubicSpline(k, Pnw, extrapolate = "True")
    def I(q):
        import numpy as np
        from scipy.special import spherical_jn as jn
        from math import pi
        lBAO = 110.
        kBAO = pi/lBAO
        S = (P(q))*(1-jn(0, q/kBAO) + 2*jn(2, q/kBAO))
        return S
    from scipy import integrate
    kmin = 0
    ks = 0.2
    S2 = integrate.quad(I, kmin, ks, limit = 100, epsabs = 0, epsrel = 10**(-5))
    import numpy as np
    from math import pi
    S2 = np.asarray(S2)
    S2 = S2*(1/(6*pi**2))
    return S2

