#Bias Parameters definition and time-evolution from Eggemeier et at 2018

def Parameters(method):
    #Initial Conditions (Local Lagrangian)

    b1L = 1. #Choice
    b2L = 0.
    beta1 = 0. #Value?
    g2L = 0.
    g2xL = 0.
    g21L = 0.
    g21xL = 0.
    g3L = 0.
    g211L = 0.
    g22L = 0.
    g31L = 0.


    #Time Evolution for bias parameters (LL included)

    if method == "Bias":
        b1 = 1. + b1L
        b2 = 0.412 -2.143*b1 + 0.929*(b1**2) + 0.008*(b1**3)
        b3 = -1.028 + 7.646*b1 - 6.227*b1**2 + 0.912*b1**3
        g2 = -(2/7)*b1L + g2L
        g2x = -(2/7)*b2 + g2xL
        g21 = 2*b1L/21 +(6/7)*g2 + g21L
        g21x = (2/21)*b2 + (6/7)*g2x + g21xL
        g3 = (-1/9)*b1L - g2 + g3L
        g211 = (5/77)*b1L + (15/14)*g2 + g21 - (9/7)*g3 + g211L
        g22 = (-6/539)*b1L - (9/49)*g2 + g22L 
        g31 = (-4/11)*b1L -6*g2 + g31L
    
    elif method == "SPT":
        b1 = 1. + b1L
        b2 = 0
        b3 = 0
        g2 = 0
        g2x = 0
        g21 = 0
        g21x = 0
        g3 = 0
        g211 = 0
        g22 = 0 
        g31 = 0
    
    return b1, b2, b3, g2, g2x, g21, g21x, g3, g211, g22, g31



def Sigma2(k, Pnw):
    from scipy.interpolate import CubicSpline
    P = CubicSpline(k, Pnw, extrapolate = "True")
    def I(q):
        import numpy as np
        from scipy.special import spherical_jn as jn
        from math import pi
        lBAO = 110.
        kBAO = pi/lBAO
        S = (P(q))*(1-jn(0, q/kBAO) + 2*jn(2, q/kBAO))
        return S
    from scipy import integrate
    kmin = 0
    ks = 0.2
    S2 = integrate.quad(I, kmin, ks, limit = 100, epsabs = 0, epsrel = 10**(-5))
    import numpy as np
    from math import pi
    S2 = np.asarray(S2)
    S2 = S2*(1/(6*pi**2))
    return S2

