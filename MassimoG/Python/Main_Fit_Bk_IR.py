
##################################  Input Cosmology ###########################################
from Cosmology import Parameters
from EuclidChallenge_Setting import MyNoWiggleCosmology, MyCosmology, EuclidChallengePk, Sigma2
method ="Bias"
dataset = "Flagship"
b1, b2, b3, g2, g2x, g21, g21x, g3, g211, g22, g31 = Parameters(method)
rangek, PkTreeDM = MyCosmology(dataset)
rangek, PknwTreeDM = MyNoWiggleCosmology(dataset) #k and Pk used for interpolations
k, Pk = EuclidChallengePk(rangek, PkTreeDM) #k and Pk used for outputs
k, Pnwk = EuclidChallengePk(rangek, PknwTreeDM) #k and Pk no-wiggle used for output


def DD(redshift):
    if redshift == "z0p9_":
        return 0.63178
    if redshift == "z1p2_":
        return 0.55868
    if redshift == "z1p5_":
        return 0.48880
    if redshift == "z1p8_":
        return 0.44589

redshift = "z1p8_"
binning = "s3_c2_"

Pk =  ((DD(redshift)))**2*Pk
Pnwk = ((DD(redshift))**2)*Pnwk

PkTreeDM =  ((DD(redshift)))**2*PkTreeDM
PknwTreeDM = ((DD(redshift))**2)*PknwTreeDM

my_cols = ["k1", "k2", "k3", "k1eff", "k2eff", "k3eff", "P(k1)", "P(k2)", "P(k3)", "B(k)", "sigma^2", "n triangle"]

import pandas 
snapshot = pandas.read_table('Data/gc-wp-nonlinear-testing-flagship_measurements-snapshots_real_space-bispectrum/flagship_measurements/snapshots_real_space/bispectrum/bispectrum_flagship_mod1_grid_540_bin_' + binning + redshift + 'full_kmax0p16.dat', 
                      names = my_cols, 
                      engine = 'python')

import numpy as np
nparticles = snapshot['k1'][0]
invdensity = snapshot['k2'][0]

k1eff = np.asarray([snapshot['k1eff'][i] for i in range(1, len(snapshot))])
k2eff = np.asarray([snapshot['k2eff'][i] for i in range(1, len(snapshot))])
k3eff = np.asarray([snapshot['k3eff'][i] for i in range(1, len(snapshot))])

Pk1 = np.asarray([snapshot['P(k1)'][i] for i in range(1, len(snapshot))])
Pk2 = np.asarray([snapshot['P(k2)'][i] for i in range(1, len(snapshot))])
Pk3 = np.asarray([snapshot['P(k3)'][i] for i in range(1, len(snapshot))])
################################## IR Leading Order Galaxy Bispectrum ##################################
import TreeLevelBispectrum_IR
from TreeLevelBispectrum_IR import C1Btree, C2Btree, C3Btree, BLOb1, BLOb2, BLOg2, Bstoch_1, Bstoch_2

S2 = Sigma2(rangek, PknwTreeDM)[0]

import numpy as np
import time 
start_time = time.time()
BkLOb1 = np.array([BLOb1(k1eff[i], k2eff[i], k3eff[i], rangek, PkTreeDM, PknwTreeDM, S2) for i in range(len(k3eff))])
BkLOb2 = np.array([BLOb2(k1eff[i], k2eff[i], k3eff[i], rangek, PkTreeDM, PknwTreeDM, S2) for i in range(len(k3eff))])
BkLOg2 = np.array([BLOg2(k1eff[i], k2eff[i], k3eff[i], rangek, PkTreeDM, PknwTreeDM, S2) for i in range(len(k3eff))])
print("--- Tree-Level Bispectrum computational time -- Flagship snapshot %s seconds ---" % (time.time() - start_time))

import pandas as pd 
MyData = {'k1eff': k1eff, 
          'k2eff': k2eff, 
          'k3eff': k3eff, 
          'BLOb1': BkLOb1, 
          'BLOb2': BkLOb2, 
          'BLOg2': BkLOg2,
          'Bsto_b1^2': Bstoch_1, 
          'Bsto_b1^2alpha1': Bstoch_1,
          'PoisStoc': Bstoch_2,
          'alpha2': Bstoch_2}

keys = ['k1eff', 'k2eff', 'k3eff', 'BLOb1', 'BLOb2', 'BLOg2', 'Bsto_b1^2', 'Bsto_b1^2alpha1', 'PoisStoc', 'alpha2']
pd.options.display.float_format = '{:.8E}'.format
df = pd.DataFrame(MyData, columns = keys)

import numpy as np
header = '%0s\t%0s\t%0s\t%0s\t%0s\t%0s\t%0s\t%0s\t%0s\t%0s\t'%('k1eff', 'k2eff', 'k3eff', 'BLOb1', 'BLOb2', 'BLOg2', 'Bsto_b1^2', 'Bsto_b1^2alpha1', 'PoisStoc', 'alpha2')

np.savetxt(r'Data/gc-wp-nonlinear-testing-flagship_measurements-snapshots_real_space-bispectrum/flagship_modelling/bispectrum_tree_IR_'+ binning + redshift + 'MassimoG.dat', df, fmt='%0.7e', header=header, comments='' )

