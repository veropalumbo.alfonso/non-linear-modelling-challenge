#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
#caricamento tabella
k,Plin,PmLO,PmNLO,Pb1b2,Pb1bG2,Pb1bGamma3,Pb2b2,Pb2bG2,PbG2bG2=np.loadtxt('power_galaxy_FS_z0p9_hres_1-loop_IRres_withoutpi_FFTlogQFT_MarcoDB.dat',unpack=True)
#interpolazioni
iPlin= interp1d(k,Plin)
iPmLO= interp1d(k,PmLO)
iPmNLO= interp1d(k,PmNLO)
iPb1b2= interp1d(k,Pb1b2)
iPb1bG2= interp1d(k,Pb1bG2)
iPb1bGamma3= interp1d(k,Pb1bGamma3)
iPb2b2= interp1d(k,Pb2b2)
iPb2bG2= interp1d(k,Pb2bG2)
iPbG2bG2= interp1d(k,PbG2bG2)


# In[2]:


plt.xscale('log')
plt.yscale('log')
plt.axis([0.001,1,10**(1),3*10**4])
plt.plot(k,Plin,'r',k, abs(PmLO), 'b', k, abs(PmNLO),'g')


# In[3]:


iPlin(k);


# In[4]:


b1=1.3162;
b2=-0.6294;
bG2=-0.2062;
bGamma3=0.2604;
c0=2.5976;


# In[5]:


modello = lambda k: b1**2*iPmNLO(k)+b1*b2*iPb1b2(k)+b1*bG2*(iPb1bG2(k)+5/2*iPb1bGamma3(k))+b1*bGamma3*iPb1bGamma3(k)+b2**2*iPb2b2(k)+b2*bG2*iPb2bG2(k)+bG2**2*iPbG2bG2(k)-2*c0*k**2*iPmLO(k)


# In[6]:


kmed,keff,Pflag,none,none2,none3=np.loadtxt('power_flagship_mod1_grid_768_bin_s1_c1_z0p9_full.dat',unpack=True, skiprows=1)


# In[17]:


residui=(Pflag-modello(keff))/Pflag


# In[20]:


plt.plot(keff,abs(residui),'b')


# In[ ]:




